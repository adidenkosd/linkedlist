﻿namespace LinkedList.Implement
{
	using System.Collections;
	using System.Collections.Generic;
	using Abstract;
	public class CustomLinkedList<T> : ICustomDoubleLinkedList<T>
	{
		private int _count;
		public void AddLast(ICustomDoubleNode<T> item) {
			if (_count == 0) {
				Head = item;
			} else {
				if (Head.Next == null) {
					Tail = item;
					Tail.Previous = Head;
					Head.Next = Tail;
				} else {
					Tail.Next = item;
					item.Previous = Tail;
					Tail = item;
				}
			}
			_count++;
		}

		public void AddFirst(ICustomDoubleNode<T> item) {
			throw new System.NotImplementedException();
		}

		public IEnumerator<ICustomDoubleNode<T>> GetEnumerator() {
			ICustomDoubleNode<T> current = Head;
			while (current.Next != null) {
				yield return current;
				current = current.Next;
			}
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}

		public void Add(ICustomDoubleNode<T> item) {
			AddLast(item);
		}

		public void Clear() {
			throw new System.NotImplementedException();
		}

		public bool Contains(ICustomDoubleNode<T> item) {
			throw new System.NotImplementedException();
		}

		public void CopyTo(ICustomDoubleNode<T>[] array, int arrayIndex) {
			throw new System.NotImplementedException();
		}

		public bool Remove(ICustomDoubleNode<T> item) {
			throw new System.NotImplementedException();
		}

		public int Count => _count;
		public bool IsReadOnly { get; }
		public ICustomDoubleNode<T> Head { get; set; }
		public ICustomDoubleNode<T> Tail { get; set; }
	}
}