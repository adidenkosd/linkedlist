﻿namespace LinkedList.Implement
{
	using Abstract;
	public class CustomLinkedListNode<T> : ICustomDoubleNode<T>
	{
		public T Value { get; }
		public ICustomDoubleNode<T> Next { get; set; }
		public ICustomDoubleNode<T> Previous { get; set; }

		public CustomLinkedListNode(T value) {
			Value = value;
		}
	}
}