﻿namespace LinkedList.Tests
{
	using Abstract;
	using Implement;
	using Xunit;

	public class CustomLinkedListTests
	{

		[Theory]
		[InlineData(new[] { 0 })]
		[InlineData(new[] { 100, 50, 600, 300 })]
		public void CanAddNodeToList(int[] values) {
			ICustomDoubleLinkedList<int> list = new CustomLinkedList<int>();
			foreach (int value in values) {
				list.Add(new CustomLinkedListNode<int>(value));
			}
			Assert.Equal(values.Length, list.Count);
		}

		[Theory]
		[InlineData(new[] { 0 })]
		[InlineData(new[] { 100, 50, 600, 300 })]
		public void CorrectBindElements(int[] values) {
			ICustomDoubleLinkedList<int> list = new CustomLinkedList<int>();
			foreach (int value in values) {
				list.Add(new CustomLinkedListNode<int>(value));
			}
			int inputArrayIndex = 0;
			foreach (ICustomDoubleNode<int> node in list) {
				Assert.Equal(values[inputArrayIndex], node.Value);
				inputArrayIndex++;
			}
			Assert.Equal(inputArrayIndex + 1, values.Length);
		}
	}
}