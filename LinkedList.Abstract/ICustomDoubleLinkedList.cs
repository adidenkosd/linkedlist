﻿namespace LinkedList.Abstract
{
	using System.Collections.Generic;
	public interface ICustomDoubleLinkedList<T> : ICollection<ICustomDoubleNode<T>>
	{
		void AddLast(ICustomDoubleNode<T> item);

		void AddFirst(ICustomDoubleNode<T> item);
	}
}