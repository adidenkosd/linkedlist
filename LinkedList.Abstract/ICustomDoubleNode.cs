﻿namespace LinkedList.Abstract
{
	public interface ICustomDoubleNode<T>
	{
		T Value {
			get;
		}

		ICustomDoubleNode<T> Next {
			get; set;
		}

		ICustomDoubleNode<T> Previous {
			get; set;
		}

	}
}